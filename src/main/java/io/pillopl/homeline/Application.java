package io.pillopl.homeline;

import io.pillopl.homeline.cache.Router;
import io.pillopl.homeline.events.Event;
import io.pillopl.homeline.events.UserFollowed;
import io.pillopl.homeline.events.UserTweeted;
import io.pillopl.homeline.events.UserUnfollowed;
import io.pillopl.homeline.cache.TweetsDistributor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableBinding(Sink.class)
public class Application  {

    private final Logger log = LoggerFactory.getLogger(Application.class.getSimpleName());

    private final TweetsDistributor cache;

    private final Router router;

    @Autowired
    public Application(TweetsDistributor readModelUpdater, Router router) {
        this.cache = readModelUpdater;
        this.router = router;
    }

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        application.run(args);
    }

    @StreamListener(Sink.INPUT)
    public void eventStream(Event event) {
        log.info("Received: " + event);

        if(event instanceof UserFollowed) {
            UserFollowed userFollowed = ((UserFollowed) event);
            router.follows(userFollowed.getFollower(), userFollowed.getFollowee());
        }

        if(event instanceof UserUnfollowed) {
            UserUnfollowed userUnfollowed = ((UserUnfollowed) event);
            router.unfollows(userUnfollowed.getFollower(), userUnfollowed.getFollowee());
        }

        if(event instanceof UserTweeted) {
            cache.distributeTweet(((UserTweeted) event));
        }
    }




}
