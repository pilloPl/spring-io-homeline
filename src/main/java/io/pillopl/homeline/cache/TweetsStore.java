package io.pillopl.homeline.cache;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TweetsStore {

    private final Map<String, List<TweetInfo>> tweets = new ConcurrentHashMap<>();

    void add(String follower, TweetInfo tweetInfo) {
       //TODO impl
    }

    public List<TweetInfo> getTweetsFor(String userName) {
        return tweets.getOrDefault(userName, Collections.emptyList());
    }

}
