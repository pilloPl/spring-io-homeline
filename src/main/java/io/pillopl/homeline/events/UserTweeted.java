package io.pillopl.homeline.events;


import java.time.Instant;

public final class UserTweeted implements Event {

    private String type = "userTweeted";

    private String authorName;
    private Long userId;
    private String body;
    private Instant when;

    public String getAuthorName() {
        return authorName;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String type() {
        return type;
    }

    public Instant getWhen() {
        return when;
    }

    public Long getUserId() {
        return userId;
    }
}
