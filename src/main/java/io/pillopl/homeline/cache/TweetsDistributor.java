package io.pillopl.homeline.cache;


import io.pillopl.homeline.events.UserTweeted;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.List;

@Component
public class TweetsDistributor {

    private final Router router;

    private final TweetsStore tweetsStore;

    private final LiveHomelines liveHomelines;

    @Autowired
    public TweetsDistributor(Router router, TweetsStore tweetsStore, LiveHomelines liveHomelines) {
        this.router = router;
        this.tweetsStore = tweetsStore;
        this.liveHomelines = liveHomelines;
    }

    public void distributeTweet(UserTweeted userTweeted) {
        router.getFollowers(userTweeted.getAuthorName()).forEach(follower ->
                addTweet(userTweeted, follower)
        );
    }

    public void follows(String follower, String followee) {
        router.follows(follower, followee);
    }

    public void unfollows(String follower, String followee) {
        router.unfollows(follower, followee);
    }

    private void addTweet(UserTweeted userTweeted, String follower) {
        TweetInfo newTweet = newTweet(userTweeted);
        tweetsStore.add(follower, newTweet);
        liveHomelines.pushTweet(follower, newTweet);

    }

    private TweetInfo newTweet(UserTweeted userTweeted) {
        return new TweetInfo(userTweeted.getBody(), userTweeted.getWhen(), userTweeted.getAuthorName());
    }
}
