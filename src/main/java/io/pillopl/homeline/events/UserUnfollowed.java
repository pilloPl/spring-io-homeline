package io.pillopl.homeline.events;


public final class UserUnfollowed implements Event {

    private final String type = "userTweeted";

    private Long followerId;
    private Long followeeId;
    private String follower;
    private String followee;

    public Long getFollowerId() {
        return followerId;
    }

    public Long getFolloweeId() {
        return followeeId;
    }

    @Override
    public String type() {
        return type;
    }

    public String getFollower() {
        return follower;
    }

    public String getFollowee() {
        return followee;
    }
}
