package io.pillopl.homeline.integration

import io.pillopl.homeline.boundary.HomelineController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.stream.messaging.Sink
import org.springframework.messaging.support.GenericMessage
import spock.lang.Ignore
import spock.lang.Subject

import java.time.Instant

import static java.time.Instant.now
import static java.time.Instant.parse

@Ignore
class E2ESpec extends IntegrationSpec {

    private static final Instant ANY_TIME = parse("1995-10-23T10:12:35Z")
    private static final Instant ANY_TIME_LATER = parse("1995-10-23T10:12:35Z").plusSeconds(3600)

    private static final Instant ANY_OTHER_TIME = ANY_TIME.plusSeconds(100)
    private static final Instant YET_ANOTHER_TIME = ANY_OTHER_TIME.plusSeconds(100)

    @Subject @Autowired HomelineController homeline

    @Autowired Sink sink

    def 'should start storing tweets when user follows'() {
        given:
            userFollowed(1, "john", 2, "ann")
        when:
            userTweeted(2, "ann", "some tweet")
        then:
            homeline.load("john").size() == 1
    }

    def 'should stop storing tweets when user unfollows'() {
        given:
            userFollowed(1, "mathieu", 2, "ann")
        when:
            userTweeted(2, "ann", "some tweet")
        then:
            homeline.load("mathieu").size() == 1
        when:
            userUnfollowed(1, "mathieu", 2, "ann")
        and:
            userTweeted(2, "ann", "some another tweet")
        then:
            homeline.load("mathieu").size() == 1

    }

    def 'should store tweets for every follower'() {
        given:
            userFollowed(1, "sergio", 2, "ann")
            userFollowed(3, "alice", 2, "ann")
            userFollowed(4, "katy", 2, "ann")
        when:
            userTweeted(2, "ann", "some tweet")
        then:
            homeline.load("sergio").size() == 1
            homeline.load("alice").size() == 1
            homeline.load("katy").size() == 1
    }

    void userFollowed(Long followerId, String followerName, Long followeeId, String followeeName) {
        sink.input().send(new GenericMessage<>(
                userFollows(followerId, followerName, followeeId, followeeName)))
    }

    void userUnfollowed(Long followerId, String followerName, Long followeeId, String followeeName) {
        sink.input().send(new GenericMessage<>(
                userUnfollows(followerId, followerName, followeeId, followeeName)))
    }

    void userTweeted(Long authorId, String authorName, String body) {
        sink.input().send(new GenericMessage<>(
                userTweets(authorId, authorName, now(), body)))
    }

    private static String userFollows(Long followerId, String followerName, Long followeeId, String followeeName) {
        return "{\"type\":\"userFollowed\",\"followerId\":\"$followerId\",\"follower\":\"$followerName\",\"followeeId\":\"$followeeId\",\"followee\":\"$followeeName\"}"
    }

    private static String userUnfollows(Long followerId, String followerName, Long followeeId, String followeeName) {
        return "{\"type\":\"userUnfollowed\",\"followerId\":\"$followerId\",\"follower\":\"$followerName\",\"followeeId\":\"$followeeId\",\"followee\":\"$followeeName\"}"
    }

    private static String userTweets(Long userId, String authorName, Instant when, String body) {
        return "{\"type\":\"userTweeted\",\"authorName\":\"$authorName\",\"userId\":\"$userId\",\"body\":\"$body\",\"when\":\"$when\"}"
    }



}
