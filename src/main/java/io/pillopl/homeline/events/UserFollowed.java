package io.pillopl.homeline.events;


public final class UserFollowed implements Event {

    private final String type = "userfollowed";

    private Long followerId;
    private Long followeeId;
    private String follower;
    private String followee;

    public Long getFollowerId() {
        return followerId;
    }

    public Long getFolloweeId() {
        return followeeId;
    }

    @Override
    public String type() {
        return type;
    }

    public String getFollowee() {
        return followee;
    }

    public String getFollower() {
        return follower;
    }
}
