package io.pillopl.homeline.cache;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class LiveHomelines {

    private final Map<String, SseEmitter> tweetsPusher = new ConcurrentHashMap<>();

    void pushTweet(String follower, TweetInfo newTweet) {
        createEmitterIfNeeded(follower);
        try {
            tweetsPusher.get(follower).send(newTweet);
        } catch (IOException e) {
            //failed to push tweet
        }
    }

    private void createEmitterIfNeeded(String follower) {
        if(!tweetsPusher.containsKey(follower)) {
            SseEmitter emmiter = new SseEmitter(-1l);
            emmiter.onCompletion(() -> tweetsPusher.remove(follower));
            tweetsPusher.putIfAbsent(follower, emmiter);

        }
    }

    public SseEmitter streamFor(String userName) {
        return tweetsPusher.get(userName);
    }
}
