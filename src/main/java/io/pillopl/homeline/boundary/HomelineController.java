package io.pillopl.homeline.boundary;

import io.pillopl.homeline.cache.LiveHomelines;
import io.pillopl.homeline.cache.TweetInfo;
import io.pillopl.homeline.cache.TweetsDistributor;
import io.pillopl.homeline.cache.TweetsStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;

@RestController
public class HomelineController {

    private final TweetsStore tweetsStore;
    private final LiveHomelines liveHomelines;

    @Autowired
    public HomelineController(TweetsStore tweetsStore, LiveHomelines liveHomelines) {
        this.tweetsStore = tweetsStore;
        this.liveHomelines = liveHomelines;
    }

    @GetMapping("/homeline/{userName}")
    public List<TweetInfo> load(@PathVariable String userName) {
        return tweetsStore.getTweetsFor(userName);
    }

    @GetMapping("/stream/{userName}")
    public SseEmitter stream(@PathVariable String userName) {
        return liveHomelines.streamFor(userName);
    }



}
