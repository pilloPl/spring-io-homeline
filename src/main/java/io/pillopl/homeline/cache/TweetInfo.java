package io.pillopl.homeline.cache;


import java.time.Instant;

public class TweetInfo {

    private final String body;
    private final Instant when;
    private final String author;

    public TweetInfo(String body, Instant when, String author) {
        this.body = body;
        this.when = when;
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public Instant getWhen() {
        return when;
    }

    public String getAuthor() {
        return author;
    }


}
