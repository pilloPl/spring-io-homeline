package io.pillopl.homeline.events;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "userFollowed", value = UserFollowed.class),
        @JsonSubTypes.Type(name = "userUnfollowed", value = UserUnfollowed.class),
        @JsonSubTypes.Type(name = "userTweeted", value = UserTweeted.class)
})
public interface Event {

    String type();
}
