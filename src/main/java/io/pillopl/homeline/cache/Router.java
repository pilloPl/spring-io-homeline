package io.pillopl.homeline.cache;

import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class Router {

    private final Map<String, Set<String>> routingMap = new ConcurrentHashMap<>();

    public void follows(String follower, String followee) {
        //TODO impl

    }

    public void unfollows(String follower, String followee) {
        //TODO impl

    }

    Set<String> getFollowers(String userName) {
        return routingMap.getOrDefault(userName, Sets.newHashSet());
    }

}
